import mongoose from 'mongoose'

const MONGO_DB = process.env.MONGO_URI

let cached = mongoose || {conn:null,promise:null}

export const connectToDatabase = async()=>{
  if(cached.conn) return cached.conn
}